#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> arrVector;
        int         sequenceArr[3];
    
        int leftMaxValIndex(int index)
        {
            int maxValIndex = -1;
            int maxValue    = 0;
            for(int i = 0 ; i < index ; i++)
            {
                if(arrVector[i] < arrVector[index] && arrVector[i] > maxValue)
                {
                    maxValIndex = i;
                    maxValue    = arrVector[maxValIndex];
                }
            }
            return maxValIndex;
        }
    
        int rightMaxValIndex(int index)
        {
            int maxValIndex = -1;
            int maxValue    = 0;
            int len         = (int)arrVector.size();
            for(int i = index+1 ; i < len ; i++)
            {
                if(arrVector[i] > arrVector[index] && arrVector[i] > maxValue)
                {
                    maxValIndex = i;
                    maxValue    = arrVector[maxValIndex];
                }
            }
            return maxValIndex;
        }
    
        void printSubsequence()
        {
            for(int i = 0 ; i < 3 ; i++)
            {
                cout<<sequenceArr[i]<<" ";
            }
            cout<<endl;
        }
    
    public:
        Engine(vector<int> aV)
        {
            arrVector = aV;
        }
    
        void findMaxProdSubsequence()
        {
            int maxProduct = 0;
            int len        = (int)arrVector.size();
            for(int i = 1 ; i < len-1 ; i++)
            {
                int leftIndex  = leftMaxValIndex(i);
                int leftValue  = leftIndex  == -1 ? 0 : arrVector[leftIndex];
                int rightIndex = rightMaxValIndex(i);
                int rightvalue = rightIndex == -1 ? 0 : arrVector[rightIndex];
                if(maxProduct < (leftValue * arrVector[i] * rightvalue))
                {
                    sequenceArr[0] = leftValue;
                    sequenceArr[1] = arrVector[i];
                    sequenceArr[2] = rightvalue;
                    maxProduct     = (leftValue * arrVector[i] * rightvalue);
                }
            }
            printSubsequence();
        }
};

int main(int argc, const char * argv[])
{
    vector<int> arrayVector = {6, 1, 2, 3, 19, 10, 7};
//    vector<int> arrayVector = {8, 9, 5, 1, 10, 3, 12};
    Engine      e           = Engine(arrayVector);
    e.findMaxProdSubsequence();
    return 0;
}
